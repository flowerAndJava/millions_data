/*
Navicat MySQL Data Transfer

Source Server         : three_port
Source Server Version : 50727
Source Host           : 10.21.28.196:3306
Source Database       : iot

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2020-12-02 13:05:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for base_dict
-- ----------------------------
DROP TABLE IF EXISTS `base_dict`;
CREATE TABLE `base_dict` (
  `dict_id` varchar(32) NOT NULL COMMENT '数据字典id(主键)',
  `dict_type_code` varchar(10) NOT NULL COMMENT '数据字典类别代码',
  `dict_type_name` varchar(64) NOT NULL COMMENT '数据字典类别名称',
  `dict_item_name` varchar(64) NOT NULL COMMENT '数据字典项目名称',
  `dict_item_code` varchar(10) DEFAULT NULL COMMENT '数据字典项目代码(可为空)',
  `dict_sort` int(10) DEFAULT NULL COMMENT '排序字段',
  `dict_enable` char(1) NOT NULL COMMENT '1:使用 0:停用',
  `dict_memo` varchar(64) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `cust_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '客户编号(主键)',
  `cust_name` varchar(32) NOT NULL COMMENT '客户名称(公司名称)',
  `cust_user_id` bigint(32) DEFAULT NULL COMMENT '负责人id',
  `cust_create_id` bigint(32) DEFAULT NULL COMMENT '创建人id',
  `cust_source` varchar(32) DEFAULT NULL COMMENT '客户信息来源',
  `cust_industry` varchar(32) DEFAULT NULL COMMENT '客户所属行业',
  `cust_level` varchar(32) DEFAULT NULL COMMENT '客户级别',
  `cust_linkman` varchar(64) DEFAULT NULL COMMENT '联系人',
  `cust_phone` varchar(64) DEFAULT NULL COMMENT '固定电话',
  `cust_mobile` varchar(16) DEFAULT NULL COMMENT '移动电话',
  `cust_zipcode` varchar(10) DEFAULT NULL,
  `cust_address` varchar(100) DEFAULT NULL,
  `cust_createtime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`cust_id`),
  KEY `FK_cst_customer_source` (`cust_source`) USING BTREE,
  KEY `FK_cst_customer_industry` (`cust_industry`) USING BTREE,
  KEY `FK_cst_customer_level` (`cust_level`) USING BTREE,
  KEY `FK_cst_customer_user_id` (`cust_user_id`) USING BTREE,
  KEY `FK_cst_customer_create_id` (`cust_create_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for iot_call_log
-- ----------------------------
DROP TABLE IF EXISTS `iot_call_log`;
CREATE TABLE `iot_call_log` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parentid` varchar(8) NOT NULL,
  `childid` varchar(4) DEFAULT NULL,
  `liftid` varchar(7) DEFAULT NULL,
  `call_no` varchar(16) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `duration` int(6) DEFAULT NULL,
  `serverdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `serverdate` (`serverdate`,`parentid`),
  KEY `parentid` (`parentid`)
) ENGINE=InnoDB AUTO_INCREMENT=7116 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iot_code
-- ----------------------------
DROP TABLE IF EXISTS `iot_code`;
CREATE TABLE `iot_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(4) NOT NULL,
  `content` varchar(60) DEFAULT NULL,
  `dev` varchar(45) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `remark` varchar(60) DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iot_dispatch
-- ----------------------------
DROP TABLE IF EXISTS `iot_dispatch`;
CREATE TABLE `iot_dispatch` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `errorid` varchar(2) NOT NULL,
  `parentid` varchar(8) NOT NULL,
  `childid` varchar(4) DEFAULT NULL,
  `liftid` varchar(7) DEFAULT NULL,
  `nowflr` varchar(2) DEFAULT NULL,
  `acd` varchar(2) DEFAULT NULL,
  `mfc_code` varchar(2) DEFAULT NULL,
  `mfc_soncode` varchar(2) DEFAULT NULL,
  `x65rg` varchar(2) DEFAULT NULL,
  `ub` varchar(4) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `serverdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `func` varchar(2) DEFAULT NULL,
  `assign_time` datetime DEFAULT NULL,
  `recover_time` datetime DEFAULT NULL,
  `recover_id` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `liftid` (`liftid`),
  KEY `assign_time` (`assign_time`),
  KEY `func` (`func`),
  KEY `errorid` (`errorid`),
  KEY `serverdate` (`serverdate`,`assign_time`,`errorid`)
) ENGINE=InnoDB AUTO_INCREMENT=8283 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iot_dispatch_history
-- ----------------------------
DROP TABLE IF EXISTS `iot_dispatch_history`;
CREATE TABLE `iot_dispatch_history` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `errorid` varchar(2) NOT NULL,
  `parentid` varchar(8) NOT NULL,
  `childid` varchar(4) DEFAULT NULL,
  `liftid` varchar(7) DEFAULT NULL,
  `nowflr` varchar(2) DEFAULT NULL,
  `acd` varchar(2) DEFAULT NULL,
  `mfc_code` varchar(2) DEFAULT NULL,
  `mfc_soncode` varchar(2) DEFAULT NULL,
  `x65rg` varchar(2) DEFAULT NULL,
  `ub` varchar(4) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `serverdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `func` varchar(2) DEFAULT NULL,
  `assign_time` datetime DEFAULT NULL,
  `recover_time` datetime DEFAULT NULL,
  `recover_id` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `liftid` (`liftid`),
  KEY `errorid` (`errorid`),
  KEY `serverdate` (`serverdate`),
  KEY `func` (`func`),
  KEY `assign_time` (`assign_time`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iot_heartbeat
-- ----------------------------
DROP TABLE IF EXISTS `iot_heartbeat`;
CREATE TABLE `iot_heartbeat` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parentid` varchar(8) NOT NULL,
  `childid` varchar(4) DEFAULT NULL,
  `liftid` varchar(7) DEFAULT NULL,
  `serverdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `device` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `serverdate` (`serverdate`),
  KEY `liftid` (`liftid`)
) ENGINE=InnoDB AUTO_INCREMENT=719870 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iot_message
-- ----------------------------
DROP TABLE IF EXISTS `iot_message`;
CREATE TABLE `iot_message` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `errorid` varchar(2) CHARACTER SET utf8 NOT NULL,
  `parentid` varchar(8) CHARACTER SET utf8 NOT NULL,
  `childid` varchar(4) CHARACTER SET utf8 DEFAULT NULL,
  `liftid` varchar(7) CHARACTER SET utf8 DEFAULT NULL,
  `nowflr` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `acd` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `mfc_code` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `mfc_soncode` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `x65rg` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `ub` varchar(4) CHARACTER SET utf8 DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `serverdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `func` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `liftid` (`liftid`),
  KEY `serverdate` (`serverdate`),
  KEY `errorid` (`errorid`),
  KEY `tcd` (`nowflr`),
  KEY `func` (`func`)
) ENGINE=InnoDB AUTO_INCREMENT=524 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for iot_monitor
-- ----------------------------
DROP TABLE IF EXISTS `iot_monitor`;
CREATE TABLE `iot_monitor` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `errorid` varchar(2) NOT NULL,
  `parentid` varchar(8) NOT NULL,
  `childid` varchar(4) DEFAULT NULL,
  `liftid` varchar(7) NOT NULL,
  `gbacd` varchar(2) DEFAULT NULL,
  `acd` varchar(2) DEFAULT NULL,
  `state1` varchar(3) DEFAULT NULL,
  `state2` varchar(2) DEFAULT NULL,
  `n900` varchar(2) DEFAULT NULL,
  `d0` varchar(2) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `n65rg` varchar(2) DEFAULT NULL,
  `serverdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `liftid` (`parentid`,`liftid`,`serverdate`)
) ENGINE=MyISAM AUTO_INCREMENT=5815 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iot_remote_data
-- ----------------------------
DROP TABLE IF EXISTS `iot_remote_data`;
CREATE TABLE `iot_remote_data` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parentid` varchar(8) NOT NULL,
  `childid` varchar(6) DEFAULT NULL,
  `liftid` varchar(7) DEFAULT NULL,
  `datapackage` varchar(500) DEFAULT NULL,
  `serverdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iot_remote_log
-- ----------------------------
DROP TABLE IF EXISTS `iot_remote_log`;
CREATE TABLE `iot_remote_log` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parentid` varchar(8) NOT NULL,
  `childid` varchar(4) DEFAULT NULL,
  `liftid` varchar(7) DEFAULT NULL,
  `func` varchar(2) DEFAULT NULL,
  `source` varchar(1) DEFAULT NULL,
  `state` int(1) DEFAULT NULL,
  `serverdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `serverdate` (`serverdate`,`parentid`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iot_synctime_log
-- ----------------------------
DROP TABLE IF EXISTS `iot_synctime_log`;
CREATE TABLE `iot_synctime_log` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parentid` varchar(8) NOT NULL,
  `childid` varchar(4) DEFAULT NULL,
  `liftid` varchar(20) DEFAULT NULL,
  `serverdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `device` varchar(2) DEFAULT NULL,
  `runt` varchar(45) DEFAULT NULL,
  `runc` varchar(45) DEFAULT NULL,
  `open` varchar(45) DEFAULT NULL,
  `distance` varchar(45) DEFAULT NULL,
  `rope` varchar(45) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `serverdate` (`serverdate`),
  KEY `parentid` (`parentid`,`liftid`,`device`)
) ENGINE=MyISAM AUTO_INCREMENT=61235 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for iot_tcd
-- ----------------------------
DROP TABLE IF EXISTS `iot_tcd`;
CREATE TABLE `iot_tcd` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parentid` varchar(8) NOT NULL,
  `childid` varchar(6) DEFAULT NULL,
  `liftid` varchar(7) DEFAULT NULL,
  `tcd` varchar(2) NOT NULL,
  `nowflr` varchar(2) DEFAULT NULL,
  `nextflr` varchar(2) DEFAULT NULL,
  `spd` varchar(2) DEFAULT NULL,
  `fml` varchar(2) DEFAULT NULL,
  `opbcall` varchar(2) DEFAULT NULL,
  `ewd` varchar(2) DEFAULT NULL,
  `comd` varchar(2) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `acd` varchar(2) DEFAULT NULL,
  `invstate` varchar(2) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `selfdefine` varchar(2) DEFAULT NULL,
  `serverdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `liftid` (`liftid`),
  KEY `tcd` (`tcd`),
  KEY `selfdefine` (`selfdefine`),
  KEY `serverdate` (`serverdate`)
) ENGINE=MyISAM AUTO_INCREMENT=1023 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for request_data
-- ----------------------------
DROP TABLE IF EXISTS `request_data`;
CREATE TABLE `request_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gprs_id` varchar(8) DEFAULT NULL,
  `gprs_flag` varchar(3) DEFAULT NULL,
  `address` varchar(40) NOT NULL,
  `port` varchar(5) DEFAULT NULL,
  `lift_id` varchar(4) DEFAULT NULL,
  `message_id` varchar(1) DEFAULT NULL,
  `func` varchar(4) NOT NULL,
  `taskNo` varchar(8) DEFAULT NULL,
  `data` varbinary(10000) DEFAULT NULL,
  `crc` varbinary(2) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `status` varchar(5) NOT NULL DEFAULT 'on',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1554 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for response_data
-- ----------------------------
DROP TABLE IF EXISTS `response_data`;
CREATE TABLE `response_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gprs_id` varchar(8) NOT NULL,
  `lift_id` varchar(4) NOT NULL,
  `task_no` varchar(7) NOT NULL,
  `func` varchar(4) NOT NULL,
  `result_data` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `status` varchar(5) NOT NULL DEFAULT 'on',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=196 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_code` varchar(32) NOT NULL COMMENT '用户账号',
  `user_name` varchar(64) NOT NULL COMMENT '用户名称',
  `user_password` varchar(32) NOT NULL COMMENT '用户密码',
  `user_state` char(1) NOT NULL COMMENT '1:正常,0:暂停',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
